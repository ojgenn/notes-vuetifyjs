// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import './stylus/main.styl'
import App from './App'
import router from './router'
import * as firebase from 'firebase'
import store from './store'

Vue.use(Vuetify)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyD7s5qwh2Jfzy99smuaH17KP71Pc-oNiPI',
      authDomain: 'vuevue-78c1c.firebaseapp.com',
      databaseURL: 'https://vuevue-78c1c.firebaseio.com',
      projectId: 'vuevue-78c1c',
      storageBucket: 'vuevue-78c1c.appspot.com',
      messagingSenderId: '1069076078208'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('loadData', user)
      } else {
        this.$store.dispatch('autoSignIn', null)
        this.$store.dispatch('loadData', null)
      }
    })
  }
})
