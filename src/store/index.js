import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    notes: []
  },
  getters: {
    getMode (state) {
      return state.mode
    },
    getUser (state) {
      return state.user
    },
    getNotes (state) {
      return state.notes
    }
  },
  mutations: {
    setMode (state, value) {
      state.mode = value
    },
    setUser (state, payload) {
      state.user = payload
    },
    userLogOut (state) {
      state.user = null
    },
    addNote (state, payload) {
      let newNote = {timestamp: payload.timestamp, noteTitle: payload.noteTitle, noteText: payload.noteText}
      state.notes.push(newNote)
    },
    addData (state, payload) {
      let key
      let newNotes = []
      for (key in payload) {
        newNotes.push({timestamp: key, noteText: payload[key].noteText, noteTitle: payload[key].noteTitle})
      }
      state.notes = newNotes
    },
    deleteNote (state, payload) {
      state.notes.splice(payload, 1)
    }
  },
  actions: {
    signUserUp ({commit}, payload) {
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            const newUser = {
              user: user.uid,
              email: payload.email
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            console.log(error.message)
          })
    },
    authUserUp ({commit}, payload) {
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            const newUser = {
              user: user.uid,
              email: payload.email
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            console.log(error.message)
          })
    },
    logUserOut ({ commit }) {
      firebase.auth().signOut()
        .then(
        () => {
          commit('userLogOut')
        }
      )
        .catch(
          error => {
            console.log(error.message)
          })
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', payload)
    },
    onAddNote ({commit}, payload) {
      firebase.database().ref(payload.ref + '/' + payload.timestamp).set({
        noteTitle: payload.noteTitle,
        noteText: payload.noteText
      })
      commit('addNote', {
        timestamp: payload.timestamp, noteTitle: payload.noteTitle, noteText: payload.noteText
      })
    },
    onDeleteNote ({commit}, payload) {
      firebase.database().ref(payload.ref + '/' + payload.timestamp).set(null)
      commit('deleteNote', payload.index)
    },
    loadData ({commit}, payload) {
      let ref
      if (payload !== null && payload !== undefined) {
        ref = payload.uid
      } else {
        ref = 'shared'
      }
      firebase.database().ref(ref).once('value')
        .then((snapshot) => {
          commit('addData', snapshot.val())
        })
    }
  }
})
