import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Auth from '@/components/Auth'
import Reg from '@/components/Reg'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth
    },
    {
      path: '/reg',
      name: 'Reg',
      component: Reg
    }
  ]
})
